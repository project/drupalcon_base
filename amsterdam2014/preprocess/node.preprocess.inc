<?php

/**
 * Implements hook_preprocess_node().
 */
function amsterdam2014_preprocess_node(&$variables) {
  $node = $variables['node'];

  $variables['theme_hook_suggestions'][] = 'node__' . $variables['view_mode'];
  $variables['theme_hook_suggestions'][] = 'node__' . $node->type;
  $variables['theme_hook_suggestions'][] = 'node__' . $node->type . '__' . $variables['view_mode'];

  if ($variables['type'] == 'news') {
    $variables['created_formatted'] = format_date($variables['created'], 'custom', 'F j, Y');
    $variables['content']['field_image']['#title'] = FALSE;
  }
}
