<?php

/**
 * Implements hook_preprocess_page().
 */
function amsterdam2014_preprocess_page(&$variables) {
  if (!empty($variables['node'])) {
    $node = $variables['node'];
    $content_type = $node->type;

    switch($content_type) {
      case 'news':
        $variables['title'] = FALSE;
        break;
    }
  }

  if (module_exists('views')) {
    if ($view = views_get_page_view()) {
      if ($view->name == 'news') {
        $variables['title'] = FALSE;
      }
    }
  }

  $status = drupal_get_http_header("status");
  if($status == "404 Not Found") {
    $variables['theme_hook_suggestions'][] = 'page__404';
  }
}
