<?php

/**
 * Implements hook_preprocess_views_view().
 */
function amsterdam2014_preprocess_views_view_list(&$variables) {
  $view = $variables['view'];
  $name = $view->name;

  if ($name == 'drupalcon_schedule') {
    $vaiables['row_attributes_array'] = 'Hello world';
    foreach ($variables['row_attributes_array'] as $delta => &$attributes) {
      $attributes['class'][] = 'track';

    }
  }
}

