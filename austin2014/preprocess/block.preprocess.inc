<?php

/**
 * Implements hook_preprocess_block().
 */
function austin2014_preprocess_block(&$variables) {
  $block = $variables['block'];
  if ($block->module == 'views' && in_array($block->delta, array('sponsor_perks-block_1', 'sponsor_perks-block_2'))) {
    $variables['attributes_array']['class'][] = 'block--primary';
  }

  // @todo use FE blocks to add a machine name or use block class module.
  if ($block->module == 'block' && $block->delta == '38') {
    $variables['attributes_array']['class'][] = 'block--primary';
  }
  if ($block->module == 'block' && $block->delta == '6') {
    $variables['attributes_array']['class'][] = 'block--tax';
  }
  if ($block->module == 'block' && $block->delta == '8') {
    $variables['attributes_array']['class'][] = 'block--refund';
  }
  if ($block->module == 'block' && $block->delta == '5') {
    $variables['attributes_array']['class'][] = 'block--footer';
  }
}
