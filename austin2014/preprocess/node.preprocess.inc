<?php

/**
 * Implements hook_preprocess_node().
 */
function austin2014_preprocess_node(&$variables) {
  $node = $variables['node'];

  $variables['theme_hook_suggestions'][] = 'node__' . $variables['view_mode'];
  $variables['theme_hook_suggestions'][] = 'node__' . $node->type;
  $variables['theme_hook_suggestions'][] = 'node__' . $node->type . '__' . $variables['view_mode'];

  if ($variables['type'] == 'news') {
    $variables['content']['links']['node']['#links']['node-readmore']['attributes']['class'][] = 'button';
    $variables['content']['links']['node']['#links']['node-readmore']['attributes']['class'][] = 'button--cursive';
    $variables['created_formatted'] = format_date($variables['created'], 'custom', 'M j');
  }

  if (isset($variables['content']['links']['drupalcon_session_evaluations_feedback'])) {
    foreach($variables['content']['links']['drupalcon_session_evaluations_feedback']['#links'] as &$link) {
      $link['attributes']['class'][] = 'button';
      $link['attributes']['class'][] = 'button--pill';
      $link['attributes']['class'][] = 'button--gamma';
      $link['attributes']['class'][] = 'button--small';
    }
  }
}
