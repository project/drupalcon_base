<?php

/**
 * @file
 * Contains a pre-process hook for 'views_view_list'.
 */

/**
 * Implements hook_preprocess_views_view_list().
 */
function austin2014_preprocess_views_view_list(&$vars) {
  
  $view = $vars['view'];
  $rows = $vars['rows'];

  if ($view->name == 'attendees') {
    foreach ($view->result as $key => $val) {
      if (empty($val->users_registration_picture)) {
        $vars['row_attributes_array'][$key]['class'][] = 'no-image';
      }
      if (empty($val->users_registration_name)) {
        $vars['row_attributes_array'][$key]['class'][] = 'no-name';
      }
    }
  }
}