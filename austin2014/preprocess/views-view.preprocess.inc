<?php

/**
 * Implements hook_preprocess_views_view().
 */
function austin2014_preprocess_views_view(&$variables) {
  $view = $variables['view'];
  $name = $view->name;
  $display = $view->current_display;

  $variables['attributes_array']['class'][] = 'view--' . drupal_clean_css_identifier($name) . '--' . drupal_clean_css_identifier($display);
}
