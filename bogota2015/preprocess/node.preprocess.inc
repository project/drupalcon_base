<?php

/**
 * Implements hook_preprocess_node().
 */
function bogota2015_preprocess_node(&$variables) {
  $node = $variables['node'];

  $variables['theme_hook_suggestions'][] = 'node__' . $variables['view_mode'];
  $variables['theme_hook_suggestions'][] = 'node__' . $node->type;
  $variables['theme_hook_suggestions'][] = 'node__' . $node->type . '__' . $variables['view_mode'];

  if ($variables['type'] == 'news') {
    $variables['created_formatted_day'] = format_date($variables['created'], 'custom', 'jS');
    $variables['created_formatted_month'] = format_date($variables['created'], 'custom', 'M');
    $variables['content']['field_image']['#title'] = FALSE;
  }
}
