<?php

/**
 * Implements hook_preprocess_views_view().
 */
function bogota2015_preprocess_views_view(&$variables) {
  $view = $variables['view'];
  $name = $view->name;
  $display = $view->current_display;

  $variables['attributes_array']['class'][] = 'view--' . drupal_clean_css_identifier($name) . '--' . drupal_clean_css_identifier($display);

  if ($name == 'drupalcon_schedule' || $name == 'personal_schedule') {
    $variables['attributes_array']['class'][] = 'view-schedule';
  }

  if ($display == 'block_diamond') {
    $row_count = count($variables['view']->result);
    if ($row_count == '2') {
      $variables['attributes_array']['class'][] = 'view-row-count-2';
    }
    if ($row_count == '4') {
      $variables['attributes_array']['class'][] = 'view-row-count-4';
    }
  }
}
