(function ($) {
  /**
   * Toggle expanded menu states.
   */
  Drupal.behaviors.dcCarousel = {
    attach: function (context) {
      $('.flexslider').flexslider();
    }
  };

})(jQuery);
