<?php

/**
 * Implements hook_preprocess_button().
 */
function drupalcon_omega_base_preprocess_button(&$variables) {

	// Add the base button class.
	$variables['element']['#attributes']['class'][] = 'button';

	// Add disabled class to buttons with disabled attribute.
	if (!empty($variables['element']['#attributes']['disabled'])) {
		$variables['element']['#attributes']['class'][] = 'button--disabled';
	}

	// Catch some positive button functions.
	foreach(array('save', 'new', 'add', 'done') as $positive) {
		if(substr_count($variables['element']['#id'], $positive)) {
		  $variables['element']['#attributes']['class'][] = 'button--positive';
		}
	}
	// Catch some negative button functions.
	foreach(array('cancel', 'remove', 'delete') as $negative) {
		if(substr_count($variables['element']['#id'], $negative)) {
		  $variables['element']['#attributes']['class'][] = 'button--negative';
		}
	}
}
