<?php

/**
 * Implements hook_preprocess_flag().
 */
function drupalcon_omega_base_preprocess_flag(&$variables) {
  // For some reason the flag classes have already been passed through drupal_attributes().
  $variables['flag_classes'] .= ' button';
}
