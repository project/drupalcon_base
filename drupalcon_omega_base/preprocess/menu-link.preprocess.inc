<?php

/**
 * Implements hook_preprocess_menu_link().
 */
function drupalcon_omega_base_preprocess_menu_link(&$variables) {
  $element = &$variables['element'];
  if ($element['#original_link']['menu_name'] == 'main-menu') {
    // Load the menu tree for this menu.
    $tree = menu_tree_all_data($element['#original_link']['menu_name'], NULL, 1);
    // Determine the target index.
    $target_index = round(count($tree) / 2) - 1;
    // Set $tree back to an indexed array so we can easily access the target
    // index.
    $tree = array_values($tree);
    // Give the menu link the spaced link attribute if it is the third link.
    if ($element['#original_link']['mlid'] == $tree[$target_index]['link']['mlid']) {
      $element['#attributes']['class'][] = 'spaced-link';
    }
  }
}
