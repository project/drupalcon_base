<?php

/**
 * Implements hook_preprocess_node().
 */
function drupalcon_omega_base_preprocess_node(&$variables) {
  $node = $variables['node'];

  if (isset($variables['content']['links']['node'])) {
    foreach($variables['content']['links']['node']['#links'] as &$link) {
      $link['attributes']['class'][] = 'button';
      $link['attributes']['class'][] = 'button--small';
    }
  }
  if (isset($variables['content']['links']['comment']) && $variables['view_mode'] == 'full') {
    foreach($variables['content']['links']['comment']['#links'] as $key => &$link) {
      if ($key == 'comment_forbidden') {
        continue;
      }
      $link['attributes']['class'][] = 'button';
    }
  }
}
