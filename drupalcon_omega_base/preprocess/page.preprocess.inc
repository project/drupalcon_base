<?php

/**
 * Implements hook_preprocess_page().
 */
function drupalcon_omega_base_preprocess_page(&$variables) {

  $variables['attributes_array']['class'][] = 'l-page';

  // Add information about the rendered sidebars, but only if the layout
  // actually supports sidebars.
  if ($matches = preg_grep('/^sidebar/', element_children($variables['page']))) {
    $count = count(array_intersect($matches, array_keys(array_filter($variables['page']))));
    // No-one is going to have more than *nine* sidebars. Even nine is actually
    // already pretty unrealistic.
    $words = array('no', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine');

    // Wrap this in a isset() just in case someone is stupid enough to have more
    // than *nine* sidebar regions.
    if (isset($words[$count])) {
      $variables['attributes_array']['class'][] = "has-{$words[$count]}-sidebar" . (($count !== 1) ? 's' : '');
    }

    foreach ($matches as $name) {
      if (!empty($variables['page'][$name])) {
        $variables['attributes_array']['class'][] = 'has-' . str_replace('_', '-', $name);
      }
    }
  }

  switch(drupal_get_path_alias()) {
    case 'speakers':
      $variables['attributes_array']['class'][] = 'has-dropped-sidebars';
      break;
  }
}
