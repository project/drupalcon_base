<?php

/**
 * Implements hook_preprocess_user_profile().
 */
function drupalcon_omega_base_preprocess_user_profile(&$variables) {
  $variables['user_profile']['info'] = array(
    '#type' => 'user_profile_category',
    '#title' => t('Profile'),
  );

  foreach($variables['user_profile'] as $key => $item) {
    // Some items we just should not touch.
    if (isset($item['#type']) && $item['#type'] == 'user_profile_category') {
      continue;
    }

    // Move anything else into the fields profile category.
    $variables['user_profile']['info'][$key] = $item;
    unset($variables['user_profile'][$key]);
  }
}
