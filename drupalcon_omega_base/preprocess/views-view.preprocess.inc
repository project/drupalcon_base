<?php

/**
 * Implements hook_preprocess_view().
 */
function drupalcon_omega_base_preprocess_views_view(&$variables) {
  $view = $variables['view'];
  $theme_path = drupal_get_path('theme', 'drupalcon_omega_base');
  if ($view->name == 'carousel' && $view->current_display == 'block') {
    drupal_add_js($theme_path . '/libraries/flexslider/jquery.flexslider-min.js');
    drupal_add_js($theme_path . '/js/carousel.js');
  }

  $view_name = $variables['attributes_array']['class'][] = drupal_clean_css_identifier('view-' . $view->name . '-' . $view->current_display);

  switch($view_name) {
    case 'view-sessions-page-proposed':
    case 'view-sessions-page-1':
      $variables['attributes_array']['class'][] = 'view-proposed-sessions';
      break;
  }
}
