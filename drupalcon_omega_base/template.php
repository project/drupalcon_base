<?php

/**
 * @file
 * Template overrides as well as (pre-)process and alter hooks for the
 * DrupalCon Omega Base theme.
 */

/**
 * Implements hook_entity_view_alter().
 */
function drupalcon_omega_base_entity_view_alter(&$build, $type) {
  if ($type == 'commerce_order') {
    // Slot the registration somewhere between the billing and line items.
    if (isset($build['commerce_customer_billing']) && isset($build['commerce_line_items']) && isset($build['commerce_registration'])) {
      $billing_weight = $build['commerce_customer_billing']['#weight'];
      $items_weight = $build['commerce_line_items']['#weight'];
      $build['commerce_registration']['#weight'] = $billing_weight + (($items_weight - $billing_weight) / 2);
    }
  }
}
