<?php

/**
 * @file
 * Template to display a list of rows.
 */
?>
<?php if (!empty($title)): ?>
  <h4><?php print $title; ?></h4>
<?php endif; ?>
<?php foreach ($rows as $delta => $row): ?>
  <div<?php print $row_attributes[$delta]; ?>>
    <?php print $row; ?>
  </div>
<?php endforeach; ?>
