/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {


// Place your code here.
  $(document).ready(function() {
    $(document).foundationMediaQueryViewer();
    $('input, textarea').placeholder();
    $(document).foundationNavigation();
    // Fix the hover styles on tablets.
    if (Modernizr.touch) {
      $('#main-menu ul.nav-bar > li').css('background', 'transparent');
      $('#main-menu ul.nav-bar a.flyout-toggle').on('click.fndtn touchstart.fndtn', function (e) {
        var parentLI = $(this).parent('li');
        parentLI.toggleClass('activenav');
        if (parentLI.hasClass('activenav')) {
          parentLI.css('background-color', '#00578C');
        }
        else {
          window.setTimeout(removeElementBG, 500, parentLI);
        }
      });
    }
  });

  function removeElementBG(element) {
    element.css('background-color', 'transparent');
  }

  Drupal.behaviors.dcpdxSlider = {
      attach: function (context, settings) {
       
          jQuery('.view-slideshow .view-content').cycle({
        		fx: 'fade', // choose your transition type, ex: fade, scrollUp, shuffle, etc...
            speed:   300, 
            timeout: 5000,
            next:   '#next', 
            prev:   '#prev',
            slideResize: false,
            containerResize: false,
            width: '100%',
            fit: 1 
        	});

          jQuery('#pause').click(function(e) {  // in jQuery 1.4 hover can accept a single fn 
              jQuery('.view-slideshow .view-content').cycle('toggle');
              var status = jQuery(this).val();
              if (status == 'Pause') { status = 'Resume'; } else { status = 'Pause'; } 
              jQuery('#pause').attr('value', status).toggleClass('paused');
          });
          
          jQuery('#prev,#next').click(function() {  
              jQuery('#pause').removeClass('paused');
          });
    
    }
  };

})(jQuery, Drupal, this, this.document);

